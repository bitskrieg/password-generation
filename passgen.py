# Simple password generator that uses the dictionary file located at ./words to create 
# multi-word passphrases.  First user argument is the number of passphrases to generate
# and second argument is the number of words in each passphrase.  There is inherent modulo
# bias here, but as long as you keep RANDMAX substantially larger than your wordlist size,
# it will not materially affect the security of your generated passphrases.

import random
import linecache
import sys

for iteration in range (0, int(sys.argv[1])):
	output = ""
	for word in range(0, int(sys.argv[2])):
		with open('words') as wordlist:
			output = output + (linecache.getline('words', (random.SystemRandom().randint(1,1000000000) % len(wordlist.readlines())))).rstrip()
	print (output)
