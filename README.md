Simple passphrase generator and wordlist.  First argument is number of passphrases to generate, second argument is number of words in the generated passphrases.  Random number generation is done using /dev/urandom on *nix and CryptGenRandom on Windows.

Default wordlist sourced from: http://www.mieliestronk.com/corncob_lowercase.txt

Words 4 characters or less in length have been removed and all words have been capitalized.